package xyz.velosh.chromedriver.scrap.commands;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.meta.api.objects.Update;
import xyz.velosh.chromedriver.scrap.TelegramBot;
import xyz.velosh.chromedriver.scrap.utils.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Get {

    /**
     * Logger: To send warning, info & errors to terminal.
     */
    private static final Logger logger = LoggerFactory.getLogger(Get.class);

    private final Update update;
    private final TelegramBot bot;

    public Get(Update update, TelegramBot telegramBot) {
        this.update = update;
        this.bot = telegramBot;
    }

    public static List<String> genericM3U8Finder(WebDriver webDriver) {
        String pageHTML = webDriver.getPageSource();
        Scanner scanner = new Scanner(pageHTML);
        List<String> list = new ArrayList<>();
        // scan entire html from page, line by line, to find something matching with m3u8, mp4...
        while (scanner.hasNext()) {
            String currentLine = scanner.next();
            if (currentLine.contains("m3u8") || (currentLine.contains("mp4")) && currentLine.length() < 300) {
                List<String> extractedUrlFromLine = extractUrls(currentLine);
                list.addAll(extractedUrlFromLine);
            }
        }
        return list;
    }

    @SuppressWarnings("all")
    public static List<String> extractUrls(String text) {
        List<String> containedUrls = new ArrayList<>();
        String urlRegex = "((https?|ftp|gopher|telnet|file):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";
        Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE);
        Matcher urlMatcher = pattern.matcher(text);
        while (urlMatcher.find()) {
            containedUrls.add(text.substring(urlMatcher.start(0), urlMatcher.end(0)));
        }
        return containedUrls;
    }

    @SuppressWarnings("SpellCheckingInspection")
    public static void scrapMe(Update update, TelegramBot bot, String text, int idd) {
        // start Chrome webdriver with parameters
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        System.setProperty("webdriver.chrome.driver", Constants.WEBDRIVER_PATH);
        WebDriver driver = new ChromeDriver(options);
        try {
            // loads URL
            driver.get(text);
            String specificFinderPossibleString = "";
            // setup some specific-site changes
            if (text.contains("animesonline.org") || text.contains("animeshouse.net")) {
                WebDriverWait wait = new WebDriverWait(driver, 10);
                wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.className("metaframe")));
            }

            JavascriptExecutor js = (JavascriptExecutor) driver;
            if (driver.getPageSource().contains("playerInstance")) {
                specificFinderPossibleString = (String) js.executeScript("return playerInstance.getPlaylist()[0].file");
            } else if (driver.getPageSource().contains("var player =")) {
                specificFinderPossibleString = (String) js.executeScript("return player.getPlaylist()[0].file");
            } else if (driver.getPageSource().contains("var jw =")) {
                specificFinderPossibleString = (String) js.executeScript("return jw.file");
            }

            String finalMessage = "<b>Metadata Results: </b>\n----\n";
            finalMessage = finalMessage + "<b>Generic Finder:</b>\n";
            List<String> candidateUrls = genericM3U8Finder(driver);

            StringBuilder genericFindersOutput = new StringBuilder();
            for (String candidateUrl : candidateUrls) {
                if (!candidateUrl.trim().isEmpty()) {
                    if (!candidateUrl.contains("\\")) {
                        if (candidateUrl.contains(";")) {
                            genericFindersOutput.append("<a href=\"").append(candidateUrl).append("\">Link</a>").append("\n");
                        } else {
                            genericFindersOutput.append("<a href=\"").append(candidateUrl.split(";")[0]).append("\">Link</a>").append("\n");
                        }
                    }
                }
            }

            finalMessage = finalMessage + genericFindersOutput;
            if (!specificFinderPossibleString.trim().isEmpty()) {
                finalMessage = finalMessage + "<b>Specific Finder:</b>\n<a href=\"" + specificFinderPossibleString + "\">Link</a>";
            }
            bot.editMessage(finalMessage.trim(), update, idd);
            driver.close();
            driver.quit();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    public void botResponse() {
        String messageUserSent = update.getMessage().getText().split(" ")[1];
        int idMessageResponse = bot.sendMessage("Working...", update);
        scrapMe(update, bot, messageUserSent, idMessageResponse);
    }
}