package xyz.velosh.chromedriver.scrap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;
import xyz.velosh.chromedriver.scrap.utils.ConfigUtil;
import xyz.velosh.chromedriver.scrap.utils.FileUtil;

import java.util.Objects;

public class Main {

    /**
     * Logger: To send warning, info & errors to terminal.
     */
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    @SuppressWarnings("SpellCheckingInspection")
    public static void main(String[] args) {
        /*
         * Check if exists props file and other things
         */
        if (!FileUtil.checkFileExistsCurPath("configs/configs.prop")) {
            logger.info("Config file not found, creating a new one...");
            new ConfigUtil().createDefConfig();
            logger.error("The properties file exists, but it does not have the correct values, fix this problem.");
            System.exit(1);
        }

        /*
         * Check if the props is ok or no
         */
        if (((ConfigUtil.getDefConfig("bot-token") != null && Objects.requireNonNull(ConfigUtil.getDefConfig("bot-token")).contains(" "))
                || (ConfigUtil.getDefConfig("bot-username") != null && Objects.requireNonNull(ConfigUtil.getDefConfig("bot-username")).contains(" ")))
                || Objects.requireNonNull(ConfigUtil.getDefConfig("solenium-chromeDriver")).contains(" ")) {
            logger.error("The properties file exists, but it does not have the correct values, fix this problem.");
            System.exit(1);
        }

        /*
         * Try to start the bot
         */
        logger.info("Trying to start the bot...");
        try {
            TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);
            botsApi.registerBot(new TelegramBot());
        } catch (TelegramApiException e) {
            logger.error(e.getMessage());
        }
    }
}