package xyz.velosh.chromedriver.scrap.utils;

@SuppressWarnings("SpellCheckingInspection")
public class Constants {
    public static String BOT_USERNAME = ConfigUtil.getDefConfig("bot-username");
    public static String BOT_TOKEN = ConfigUtil.getDefConfig("bot-token");
    public static String WEBDRIVER_PATH = ConfigUtil.getDefConfig("solenium-chromeDriver");
}