package xyz.velosh.chromedriver.scrap.utils;

import java.io.File;

public class FileUtil {

    public static boolean checkIfFolderExists(String folder) {
        File file = new File(folder);
        return file.exists() && file.isDirectory();
    }

    public static boolean createFolder(String folder) {
        if (!checkIfFolderExists(folder)) {
            File dir = new File(folder);
            return dir.mkdir();
        }
        return false;
    }

    public static boolean checkFileExistsCurPath(String file) {
        File f = new File(file);
        return f.exists() && !f.isDirectory();
    }
}