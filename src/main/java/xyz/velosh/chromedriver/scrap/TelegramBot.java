package xyz.velosh.chromedriver.scrap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import xyz.velosh.chromedriver.scrap.commands.Get;
import xyz.velosh.chromedriver.scrap.utils.Constants;

import java.util.concurrent.ExecutionException;

public class TelegramBot extends TelegramLongPollingBot {

    /**
     * Logger: To send warning, info & errors to terminal.
     */
    private static final Logger logger = LoggerFactory.getLogger(TelegramBot.class);

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            if (update.getMessage().getText().startsWith("!get")) {
                new Get(update, this).botResponse();
            }
        }
    }

    @Override
    public String getBotUsername() {
        return Constants.BOT_USERNAME;
    }

    @Override
    public String getBotToken() {
        return Constants.BOT_TOKEN;
    }

    public int sendMessage(String msg, Update update) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setText(msg);
        sendMessage.setChatId(String.valueOf(update.getMessage().getChatId()));
        sendMessage.enableMarkdown(true);
        sendMessage.disableWebPagePreview();

        try {
            return executeAsync(sendMessage).get().getMessageId();
        } catch (TelegramApiException | InterruptedException | ExecutionException e) {
            logger.error(e.getMessage());
            return 0;
        }
    }

    public void editMessage(String msg, Update update, int id) {
        EditMessageText editMessageText = new EditMessageText();
        editMessageText.setText(msg);
        editMessageText.setChatId(String.valueOf(update.getMessage().getChatId()));
        editMessageText.setMessageId(id);
        editMessageText.enableHtml(true);

        try {
            executeAsync(editMessageText);
        } catch (TelegramApiException e) {
            logger.error(e.getMessage());
        }
    }
}